﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Paper_Down_Under
{
    public class Room
    {
        public int num;
        public int up, right, down, left;
        public Texture2D background;
        public Tile[][] tileMap;
        private Camera camera;
        public List<Enemy> enemies;
        public Boolean isBattle = false;
        public Rectangle startPos;

        public Room(int _num, int _up, int _right, int _down, int _left, Texture2D _background, Tile[][] _tileMap, Camera _camera)
        {
            num = _num;
            up = _up;
            right = _right;
            down = _down;
            background = _background;
            tileMap = _tileMap;
            camera = _camera;
            enemies = new List<Enemy>();
        }

        public void draw(Player player, GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, camera.viewport, Color.White);
            foreach (Tile[] tiles in tileMap)
            {
                foreach (Tile tile in tiles)
                {
                    if (tile != null)
                    {
                        tile.draw(gameTime, spriteBatch, camera);
                    }
                }
            }
            //Console.WriteLine(enemies.Count);
            foreach (Enemy enemy in enemies)
            {
                
                if(!isBattle)
                {
                    enemy.update();
                    enemy.move(this, player);
                    enemy.draw(gameTime, spriteBatch);
                }
                else if (enemy.inBattle())
                {
                    enemy.battleAnim(gameTime);
                    enemy.draw(gameTime, spriteBatch);
                }

            }
        }
        //Battle stuff below
        public Enemy intercept(Player p)
        {
            Enemy e = null;
            foreach (Enemy enemy in enemies)
            {
                if (p.ploc.Intersects(enemy._loc))
                {
                    e = enemy;
                    isBattle = true;
                    enemy.setBattle(true);
                    break;
                }
            }
            return e;
        }
        public int enemyFight(GameTime gameTime)
        {
            int i = 0;
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    i = enemy.attack();
                }
            }
            return i;
        }
        public Boolean enemyLife()
        {
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    if (!enemy.isAlive())
                    {
                        enemies.Remove(enemy);
                        return false;
                    }
                }
            }
            return true;
        }
        public int dmgDealt(int pATK)
        {
            int ret = 0;
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    ret = enemy.damaged(pATK);
                }
            }
            return ret;
        }
        public Boolean isTurn()
        {
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    return enemy.turn;
                }
            }
            return false;
        }
        public void setTurn(Boolean b)
        {
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    enemy.turn = b;
                }
            }
        }
        public void noBattle()
        {
            isBattle = false;
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    enemy.setBattle(false);
                }
            }
        }
        public Boolean hit()
        {
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    if (enemy.atkAnim <= 60)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public void setRect(Rectangle r)
        {
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    enemy.setLoc(r);
                }
            }
        }
        public Rectangle getRect()
        {
            Rectangle r = new Rectangle();
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    r = enemy._loc;
                }
            }
            return r;
        }
        public void battleDraw(GameTime gT, SpriteBatch sB, SpriteFont sf)
        {
            foreach (Enemy enemy in enemies)
            {
                if (enemy.inBattle())
                {
                    enemy.battleDraw(gT, sB, sf);
                }
            }
        }

    }
}
