﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Paper_Down_Under
{
    public abstract class GameObject
    {
        protected Animation idleAnim;
        protected Rectangle loc;
        protected Vector2 pos;
        protected Vector2 vel;
        protected Camera camera;

        public GameObject(Animation _idleAnim, Rectangle _loc, Camera _camera)
        {
            idleAnim = _idleAnim;
            loc = _loc;
            pos = new Vector2(loc.X, loc.Y);
            vel = new Vector2(0, 0);
            camera = _camera;
        }

        public Rectangle _loc
        {
            get
            {
                return loc;
            }
            set
            {
                loc = value;
            }
        }

        public Vector2 _pos
        {
            get
            {
                return pos;
            }
            set
            {
                pos = value;
            }
        }

        public Vector2 _vel
        {
            get
            {
                return vel;
            }
            set
            {
                vel = value;
            }
        }

        public void update()
        {
            pos.X += vel.X;
            pos.Y += vel.Y;
            loc.X = (int)Math.Round(pos.X) - camera.loc.X;
            loc.Y = (int)Math.Round(pos.Y) - camera.loc.Y;
            idleAnim.next();
        }

        public void draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (vel.X <= 0)
            {
                spriteBatch.Draw(idleAnim.getFrame(), loc, Color.White);
            }
            else
            {
                spriteBatch.Draw(idleAnim.getFrame(), loc, null, Color.White, 0, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 0);
            }
        }
    }
}
