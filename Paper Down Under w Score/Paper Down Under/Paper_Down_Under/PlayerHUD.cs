﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace Paper_Down_Under
{
    public class PlayerHUD
    {
        public int hP { get; set; }
        public int def { get; set; }
        public int acc { get; set; }
        public int atk { get; set; }
        public int gold { get; set; }
        public int maxHP { get; set; }
        public int maxTP { get; set; }
        public int tP { get; set; }
        public int exp;
        public int expReq;
        public int lvl;
        Texture2D hpBarText;
        Texture2D tpBarText;
        Rectangle hpBarRect;
        Rectangle hpLeftBarRect;
        Rectangle tpBarRect;
        Rectangle tpLeftBarRect;
        Texture2D hpIconText;
        Texture2D tpIconText;
        Texture2D gIconText;
        Texture2D playerIconText;
        Texture2D playerBackIconText;
        Rectangle playerIconRect;
        Rectangle hpIconRect;
        Rectangle tpIconRect;
        Rectangle gIconRect;
        SpriteFont sF;
        public int lvlTimer = 0;

        public PlayerHUD(int health, int defense, int accuracy, int attack, int tension)
        {
            hP = health;
            maxHP = hP;
            def = defense;
            acc = accuracy;
            atk = attack;
            tP = tension;
            maxTP = tP;
            gold = 10;
            lvl = 1;
            expReq = 5;
        }

        public void LC(SpriteFont spriteFont, Texture2D healthBar, Texture2D tensionBar, Texture2D healthIcon, Texture2D tensionIcon, Texture2D coinIcon, Texture2D player, Texture2D playerBack)
        {
            sF = spriteFont;
            hpBarText = healthBar;
            tpBarText = tensionBar;
            hpBarRect = new Rectangle(200, 25, 300, 50);
            hpLeftBarRect = new Rectangle(hpBarRect.X, hpBarRect.Y, (int)(hpBarRect.Width * ((float)hP / maxHP)), hpBarRect.Height);
            hpIconRect = new Rectangle(150, 10, 75, 75);
            tpBarRect = new Rectangle(200, 100, 250, 40);
            tpLeftBarRect = new Rectangle(tpBarRect.X, tpBarRect.Y, (int)(tpBarRect.Width * ((float)tP / maxTP)), tpBarRect.Height);
            tpIconRect = new Rectangle(170, 85, 55, 55);
            hpIconText = healthIcon;
            tpIconText = tensionIcon;
            playerIconText = player;
            playerBackIconText = playerBack;
            playerIconRect = new Rectangle(10, 15, 145, 145);
            gIconText = coinIcon;
            gIconRect = new Rectangle(170, 145, 55, 55);
        }
        public void canLevel()
        {
            
            if (exp/expReq >= 1)
            {
                exp -= expReq;
                
                levelUp();
            }
        }
        public void levelUp()
        {
            lvl++;
            expReq += 3;
            maxHP += 10;
            atk+= 1;
            if (lvl % 2 == 0)
            {
                def+= 1;
                atk += 1;
                maxTP++;
            }
            if (lvl % 5 == 0)
            {
                expReq += 2;
                acc++;
                def += 1;
                maxHP += 5;
            }
            lvlTimer = 150;
        }
        public void death()
        {
            exp = 0;
            gold -= 30;
            if (gold < 0)
            {
                gold = 0;
            }
            hP = maxHP;
            tP = maxTP;
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            lvlTimer--;
            KeyboardState kB = Keyboard.GetState();
            hpLeftBarRect = new Rectangle(hpBarRect.X, hpBarRect.Y, (int)(hpBarRect.Width * ((float)hP / maxHP)), hpBarRect.Height);
            tpLeftBarRect = new Rectangle(tpBarRect.X, tpBarRect.Y, (int)(tpBarRect.Width * ((float)tP / maxTP)), tpBarRect.Height);
            spriteBatch.Draw(playerBackIconText, playerIconRect, Color.White);
            spriteBatch.Draw(playerIconText, playerIconRect, Color.White);
            spriteBatch.Draw(hpBarText, hpBarRect, Color.DarkRed);
            spriteBatch.Draw(tpBarText, tpBarRect, Color.DarkBlue);
            spriteBatch.Draw(hpBarText, hpLeftBarRect, new Color(255, 100, 100));
            spriteBatch.Draw(tpBarText, tpLeftBarRect, new Color(100, 100, 255));
            spriteBatch.Draw(hpIconText, hpIconRect, Color.White);
            spriteBatch.Draw(tpIconText, tpIconRect, Color.White);
            spriteBatch.Draw(gIconText, gIconRect, Color.White);
            
            spriteBatch.DrawString(sF, hP + "/" + maxHP, new Vector2(250, 25), Color.White);
            spriteBatch.DrawString(sF, tP + "/" + maxTP, new Vector2(250, 95), Color.White);
            spriteBatch.DrawString(sF, "" + gold, new Vector2(230, 145) , Color.White);
            spriteBatch.DrawString(sF, "Level: " + lvl, new Vector2(950, 10), Color.Aqua);
            if (lvlTimer > 0 )
            {
                spriteBatch.DrawString(sF, "Level Up!", new Vector2(530, 250), Color.Aqua);
            }
            //Below is for the extra stats that aren't going to be drawn
            if (kB.IsKeyDown(Keys.Tab))
            {
                spriteBatch.DrawString(sF, "Exp: " + exp + "/" + expReq, new Vector2(40, 190), Color.White);
                spriteBatch.DrawString(sF, "Attack: " + atk, new Vector2(40, 230), Color.White);
                spriteBatch.DrawString(sF, "Defense: " + def, new Vector2(40, 270), Color.White);
                spriteBatch.DrawString(sF, "Accuracy: " + acc + "%", new Vector2(40, 310), Color.White);
            }

        }
    }
}
