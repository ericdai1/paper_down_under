﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Paper_Down_Under
{

    public class Enemy : GameObject
    {
        public enum Status
        {
            None,
            Poisoned,
            Burned,
            Paralyzed,
            Cursed
        }

        public enum Movement
        {
            Wander,
            Follow,
            Battle
        }

        public enum Type
        {
            Flying,
            Ground
        }

        public int hP { get; set; }
        public int def { get; set; }
        public int acc { get; set; }
        public int atk { get; set; }
        public string name { get; set; }
        public int gold { get; set; }
        public int exp { get; set; }
        public Movement movement { get; set; }
        public Movement movementFar { get; set; }
        public Movement movementClose { get; set; }
        public Type typ;
        private Status status = Status.None;
        private Random random;
        private int xTimer, yTimer;
        private int maxXTimer, maxYTimer;
        private int proximity;
        private Vector2 baseVel;
        public int atkAnim = 0;
        public Boolean turn = false;
        public int maxHP;
        private Rectangle pL;

        //sX = pixels horizontally for each sprite in the sheet
        //sY = height of spritesheet
        //count = # of images in spritesheet
        public Enemy(int h, int d, int ac, int at, int g, int ex, string n, Animation _idleAnim, Rectangle _loc, Camera _camera, Movement moFar, Movement moClo, int pro, Vector2 bv, Random ran) : base(_idleAnim, _loc, _camera)
        {
            maxHP = h;
            hP = h;
            def = d;
            acc = ac;
            atk = at;
            name = n;
            gold = g;
            exp = ex;

            movement = moFar;
            movementFar = moFar;
            movementClose = moClo;
            proximity = pro;
            baseVel = bv;
            random = ran;
            

            if (movement == Movement.Wander)
            {
                if (random.Next(2) == 0)
                {
                    vel.X = 1f;
                }
                else
                {
                    vel.X = -1f;
                }

                if (random.Next(2) == 0)
                {
                    vel.Y = 1f;
                }
                else
                {
                    vel.Y = -1f;
                }

                maxXTimer = random.Next(250, 500);
                maxYTimer = random.Next(250, 500);
                xTimer = 0;
                yTimer = 0;
            }
        }

        //poison, burn, etc.
        public void setStatus(Status _status, int dmg, int turns)
        {
            status = _status;
        }

        //when player attacks the enemy
        public int damaged(int dmg)
        {
            if (dmg <= def)
            {
                return 0;
            }
            hP = hP - (dmg - def);
            return (dmg - def);
        }

        public void move(Room curr, Player player)
        {
            Vector2 displacement = new Vector2(loc.Center.X - player._loc.Center.X, loc.Center.Y - player._loc.Center.Y);
            if (displacement.X == 0)
            {
                displacement.X = .1f;
            }
            if (displacement.Y == 0)
            {
                displacement.Y = .1f;
            }
            float distance = (float)Math.Abs(Math.Sqrt(displacement.X * displacement.X + displacement.Y * displacement.Y));
            if (distance <= proximity)
            {
                movement = movementClose;
            }
            else
            {
                movement = movementFar;
            }
            displacement.Normalize();

            if (movement == Movement.Wander)
            {
                if (loc.X + camera.loc.X <= 0 && vel.X < 0)
                {
                    xTimer = 0;
                    vel.X = -vel.X;
                    maxXTimer = random.Next(250, 500);
                }
                if (loc.X + camera.loc.X >= curr.tileMap.Length * Game1.TileLength && vel.X > 0)
                {
                    xTimer = 0;
                    vel.X = -vel.X;
                    maxXTimer = random.Next(250, 500);
                }
                if (loc.Y + camera.loc.Y >= curr.tileMap[0].Length * Game1.TileLength && vel.Y > 0)
                {
                    yTimer = 0;
                    vel.Y = -vel.Y;
                    maxYTimer = random.Next(250, 500);
                }
                if (loc.Y + camera.loc.Y <= 0 && vel.Y < 0)
                {
                    yTimer = 0;
                    vel.Y = -vel.Y;
                    maxYTimer = random.Next(250, 500);
                }

                xTimer++;
                yTimer++;
                if (xTimer >= maxXTimer)
                {
                    xTimer = 0;
                    if (vel.X < 0)
                    {
                        vel.X = baseVel.X;
                    }
                    else
                    {
                        vel.X = -baseVel.X;
                    }
                    maxXTimer = random.Next(250, 500);
                }
                if (yTimer >= maxYTimer)
                {
                    yTimer = 0;
                    if (vel.Y < 0)
                    {
                        vel.Y = baseVel.Y;
                    }
                    else
                    {
                        vel.Y = -baseVel.Y;
                    }
                    maxYTimer = random.Next(250, 500);
                }
            }
            else if (movement == Movement.Follow)
            {
                vel.X = -displacement.X * baseVel.X * 2;
                vel.Y = -displacement.Y * baseVel.Y * 2;
            }
        }
        public void setLoc(Rectangle pLoc)
        {
            pL = pLoc;
            loc.X = pLoc.X + 100;
            loc.Y = pLoc.Y;
        }
        public void battleAnim(GameTime gameTime)
        {
            if (atkAnim > 60)
            {
                loc.X -= 2;
                
            }
            else if (atkAnim > 40)
            {
                
            }
            else if (atkAnim > 0)
            {
                loc.X += 2;
            }
            atkAnim--;
        }
        public Boolean inBattle()
        {
            if (movement == Movement.Battle)
            {
                return true;
            }
            return false;
        }
        public void setBattle(Boolean b)
        {
            if (b)
            {
                movement = Movement.Battle;
                vel.X = 0f;
            }

            else
                movement = Movement.Wander;
        }
        public int attack()
        {
            
            if (status == Status.None)
            {
                atkAnim = 100;
                loc.X = pL.X + 100;
                Random rand = new Random();
                int r = rand.Next(100);
                if (r < acc)
                {
                    
                    return atk;
                }
            }
            
            return 0;

        }
        public Boolean isAlive()
        {
            if (hP <= 0)
            {
                return false;
            }
            return true;
        }
        public void battleDraw(GameTime gameTime, SpriteBatch spriteBatch, SpriteFont sf)
        {
            spriteBatch.DrawString(sf, hP + "/" + maxHP, new Vector2(loc.X + 20, loc.Y - 30), Color.White);
        }
    }
}