﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace Paper_Down_Under
{
    public class Player : GameObject
    {
        private Vector2 baseVel;
        private Vector2 acceleration;
        private KeyboardState kb;
        private Boolean isRight;
        private Boolean isCrouching;
        private Boolean isJumping;
        private Animation walkAnim;
        private Animation crouchAnim;
        private Animation jumpAnim;
        private Texture2D hurtAnim;
        private Texture2D faintAnim;
        private Boolean canJump;
        const int TileLength = 64;
        public Rectangle ploc;
        private SpriteFont debugFont;
        Tile cTile;
        private const float VelStop = 1.5f;
        private const int walkSpeed = 10;
        private const float decceleration = .3f;
        //Battle variables
        public int lives = 3;
        private Boolean isAlive = true;
        public PlayerHUD hud;
        public Boolean inBattle = false;
        public KeyboardState oldKB = Keyboard.GetState();
        public Boolean turn = false;
        public Boolean isAttacking = false;
        public Boolean itemsOpen = false;
        public Boolean hurt = false;
        public Boolean faint = false;
        public Boolean isRetreating = false;
        public Boolean hightlightFight = false;
        public Boolean hightlightItem = false;
        public Boolean highlightFlee = false;
        public Boolean gameOver = false;
        private int ticks = 0;
        private int atkAnim = 0;


        //i added hurt & faint textures
        public Player(Animation _idleAnim, Animation _walkAnim, Animation _jumpAnim, Animation _crouchAnim, Texture2D _hurtAnim, Texture2D _faintAnim, SpriteFont _debugFont, Rectangle _loc, Camera _camera, Vector2 _baseVel, Vector2 _acceleration) : base(_idleAnim, _loc, _camera)
        {
            baseVel = _baseVel;
            acceleration = _acceleration;
            kb = Keyboard.GetState();
            walkAnim = _walkAnim;
            jumpAnim = _jumpAnim;
            crouchAnim = _crouchAnim;
            hurtAnim = _hurtAnim;
            faintAnim = _faintAnim;
            isCrouching = false;
            isJumping = false;
            lives = 3;
            ploc = new Rectangle(loc.X + 18, loc.Y + 15, loc.Width - 36, loc.Height - 15);
            debugFont = _debugFont;
        }
        public void InitializeHUD(PlayerHUD hud)
        {
            this.hud = hud;
        }
        public void update(GameTime gameTime, Room curr)
        {
            kb = Keyboard.GetState();
            Vector2 dir = new Vector2();
            ticks++;
            if (canJump)
            {
                if (kb.IsKeyDown(Keys.Up))
                {
                    isJumping = true;
                    dir.Y -= 1;
                }
                else
                {
                    isJumping = false;
                    canJump = false;

                }
            }

            if (kb.IsKeyDown(Keys.Right))
            {
                dir.X += 1;
                isRight = true;
            }
            //To be Added once Collisions are fixed
            if (kb.IsKeyDown(Keys.Down))
            {
                dir.Y += 1;
                //isCrouching = true;
            }
            else if (vel.Y == 0)
            {
                //isCrouching = false;
            }
            if (kb.IsKeyDown(Keys.Left))
            {
                dir.X -= 1;
                isRight = false;
            }

            if (dir.Y < 0)
            {
                vel.Y -= 2 * acceleration.Y;
                if (vel.Y < -(2 * baseVel.Y / 3))
                {
                    vel.Y = -(2 * baseVel.Y / 3);
                    canJump = false;
                }
            }
            if (dir.X < 0 && vel.X > -baseVel.X)
            {
                vel.X -= acceleration.X;
                if (vel.X < -baseVel.X)
                {
                    vel.X = -baseVel.X;
                }
            }
            else if (dir.X > 0 && vel.X < baseVel.X)
            {
                vel.X += acceleration.X;
                if (vel.X > baseVel.X)
                {
                    vel.X = baseVel.X;
                }
            }

            if (dir.X == 0)
            {
                if (vel.X > 0)
                {
                    vel.X -= decceleration;
                    if (vel.X < 0)
                    {
                        vel.X = 0;
                    }
                }
                else if (vel.X < 0)
                {
                    vel.X += decceleration;
                    if (vel.X > 0)
                    {
                        vel.X = 0;
                    }
                }
            }

            vel.Y += (acceleration.Y / 2);
            if (vel.Y > baseVel.Y)
            {
                vel.Y = baseVel.Y;
            }

            Rectangle oldLoc = camera.loc;
            camera.loc.X += (int)vel.X;
            camera.loc.Y += (int)vel.Y;

            Tile[][] map = curr.tileMap;

            for (int r = 0; r < map.Length; r++)
            {
                for (int c = 0; c < map[r].Length; c++)
                {
                    if (map[r][c] != null && (map[r][c]._state == Tile.TileState.Platform || map[r][c]._state == Tile.TileState.Impassable))
                    {
                        Rectangle nLoc = new Rectangle(map[r][c]._loc.X - camera.loc.X,
                                                       map[r][c]._loc.Y - camera.loc.Y,
                                              map[r][c]._loc.Width, map[r][c]._loc.Height);
                        if (vel.Y > 0 && ploc.TouchTopOf(nLoc))
                        {
                            if (map[r][c] == cTile && cTile != null)
                            {
                                continue;
                            }
                            else if (map[r][c] != cTile && cTile != null)
                            {
                                isCrouching = false;
                                cTile = null;
                            }
                            vel.Y = -(vel.Y / 5);
                            camera.loc.Y = oldLoc.Y;
                            canJump = true;
                            if (kb.IsKeyDown(Keys.Down) && map[r][c]._state == Tile.TileState.Platform && ploc.TouchTopOf(nLoc))
                            {
                                vel.Y = 1f;
                                isCrouching = true;
                                cTile = map[r][c];
                            }
                        }
                        if (map[r][c]._state == Tile.TileState.Impassable && ploc.TouchBottomOf(nLoc))
                        {
                            vel.Y = 1f;
                            canJump = false;
                        }
                        if (map[r][c]._state == Tile.TileState.Impassable && vel.X > 0 && ploc.TouchLeftOf(nLoc))
                        {
                            vel.X = -(vel.X / 3);
                            camera.loc.X = oldLoc.X;
                        }
                        if (map[r][c]._state == Tile.TileState.Impassable && vel.X < 0 && ploc.TouchRightOf(nLoc))
                        {
                            vel.X = -(vel.X / 3);
                            camera.loc.X = oldLoc.X;
                        }
                        
                    }
                    if (map[r][c] != null && map[r][c]._state == Tile.TileState.Fluid)
                    {
                        Rectangle nLoc = new Rectangle(map[r][c]._loc.X - camera.loc.X,
                                                       map[r][c]._loc.Y - camera.loc.Y,
                                              map[r][c]._loc.Width, map[r][c]._loc.Height);
                        if (ploc.Intersects(nLoc))
                        {
                            vel.Y -= .4f;
                        }
                    }
                }
            }
            if (hud.tP < hud.maxTP)
            {
                if (ticks % 360 == 0 && !inBattle)
                {
                    hud.tP++;

                }
            }
            if (hud.hP < hud.maxHP)
            {
                if (ticks % 100 == 0 && !inBattle)
                {
                    hud.hP+= hud.maxHP/50;

                }
            }

            camera.loc.X += (int)vel.X;
            camera.loc.Y += (int)vel.Y;
            if (isJumping)
            {
                jumpAnim.next();
            }
            else
            {
                jumpAnim.last();
            }
            if (isCrouching)
            {
                crouchAnim.next();
            }
            else
            {
                crouchAnim.last();
            }
            if (vel.X > -VelStop && vel.X < VelStop)
            {
                idleAnim.next();
                walkAnim.last();
            }
            else
            {
                walkAnim.imgTick = (int)Math.Abs(Math.Round(walkSpeed / (vel.X / 2)));
                walkAnim.next();
                idleAnim.last();
            }
            hud.canLevel();
        }
        //battle interface stuff below
        public void battleAnim(GameTime gameTime, Room curr)
        {
            KeyboardState kB = Keyboard.GetState();
            Vector2 dir = new Vector2();
            ticks++;
            

            if (dir.Y < 0)
            {
                vel.Y -= 2 * acceleration.Y;
                if (vel.Y < -(2 * baseVel.Y / 3))
                {
                    vel.Y = -(2 * baseVel.Y / 3);
                }
            }
            if (dir.X < 0 && vel.X > -baseVel.X)
            {
                vel.X -= acceleration.X;
                if (vel.X < -baseVel.X)
                {
                    vel.X = -baseVel.X;
                }
            }
            else if (dir.X > 0 && vel.X < baseVel.X)
            {
                vel.X += acceleration.X;
                if (vel.X > baseVel.X)
                {
                    vel.X = baseVel.X;
                }
            }

            if (dir.X == 0)
            {
                if (vel.X > 0)
                {
                    vel.X -= decceleration;
                    if (vel.X < 0)
                    {
                        vel.X = 0;
                    }
                }
                else if (vel.X < 0)
                {
                    vel.X += decceleration;
                    if (vel.X > 0)
                    {
                        vel.X = 0;
                    }
                }
            }

            vel.Y += (acceleration.Y / 2);
            if (vel.Y > baseVel.Y)
            {
                vel.Y = baseVel.Y;
            }

            Rectangle oldLoc = camera.loc;
            camera.loc.X += (int)vel.X;
            camera.loc.Y += (int)vel.Y;

            Tile[][] map = curr.tileMap;

            for (int r = 0; r < map.Length; r++)
            {
                for (int c = 0; c < map[r].Length; c++)
                {
                    if (map[r][c] != null && (map[r][c]._state == Tile.TileState.Platform || map[r][c]._state == Tile.TileState.Impassable))
                    {
                        Rectangle nLoc = new Rectangle(map[r][c]._loc.X - camera.loc.X,
                                                       map[r][c]._loc.Y - camera.loc.Y,
                                              map[r][c]._loc.Width, map[r][c]._loc.Height);
                        if (vel.Y > 0 && ploc.TouchTopOf(nLoc))
                        {
                            if (map[r][c] == cTile && cTile != null)
                            {
                                continue;
                            }
                            else if (map[r][c] != cTile && cTile != null)
                            {
                                isCrouching = false;
                                cTile = null;
                            }
                            vel.Y = -(vel.Y / 5);
                            camera.loc.Y = oldLoc.Y;
                            canJump = true;
                            if (kb.IsKeyDown(Keys.Down) && map[r][c]._state == Tile.TileState.Platform && ploc.TouchTopOf(nLoc))
                            {
                                vel.Y = 1f;
                                isCrouching = true;
                                cTile = map[r][c];
                            }
                        }
                        if (map[r][c]._state == Tile.TileState.Impassable && ploc.TouchBottomOf(nLoc))
                        {
                            vel.Y = 1f;
                            canJump = false;
                        }
                        if (map[r][c]._state == Tile.TileState.Impassable && vel.X > 0 && ploc.TouchLeftOf(nLoc))
                        {
                            vel.X = -(vel.X / 3);
                            camera.loc.X = oldLoc.X;
                        }
                        if (map[r][c]._state == Tile.TileState.Impassable && vel.X < 0 && ploc.TouchRightOf(nLoc))
                        {
                            vel.X = -(vel.X / 3);
                            camera.loc.X = oldLoc.X;
                        }

                    }
                    if (map[r][c] != null && map[r][c]._state == Tile.TileState.Fluid)
                    {
                        Rectangle nLoc = new Rectangle(map[r][c]._loc.X - camera.loc.X,
                                                       map[r][c]._loc.Y - camera.loc.Y,
                                              map[r][c]._loc.Width, map[r][c]._loc.Height);
                        if (ploc.Intersects(nLoc))
                        {
                            vel.Y -= .4f;
                        }
                    }
                }
            }
            if (hud.tP < hud.maxTP)
            {
                if (ticks % 300 == 0 && !inBattle)
                {
                    hud.tP++;

                }
            }
            if (hud.hP < hud.maxHP)
            {
                if (ticks % 60 == 0 && !inBattle)
                {
                    hud.hP++;

                }
            }

            camera.loc.X += (int)vel.X;
            camera.loc.Y += (int)vel.Y;
           
            if (isJumping)
            {
                jumpAnim.next();
            }
            else
            {
                jumpAnim.last();
            }
            if (isCrouching)
            {
                crouchAnim.next();
            }
            else
            {
                crouchAnim.last();
            }
            if (vel.X > -VelStop && vel.X < VelStop)
            {
                idleAnim.next();
                walkAnim.last();
            }
            else
            {
                walkAnim.imgTick = (int)Math.Abs(Math.Round(walkSpeed / (vel.X / 2)));
                walkAnim.next();
                idleAnim.last();
            }
            if (turn)
            {
                if (kB.IsKeyDown(Keys.A) && !oldKB.IsKeyDown(Keys.A))
                {
                    isAttacking = true;
                }
                else if (kB.IsKeyDown(Keys.B) && !oldKB.IsKeyDown(Keys.B))
                {
                    isRetreating = true;
                }
                oldKB = kB;
            
            }
            battleAnim2();

            //hurtAnim if attacked
            //idleAnim.next();
        }
        public void battleAnim2()
        {
            if (atkAnim > 30)
            {
               loc.X += 4;

            }
            else if (atkAnim > 20)
            {
                jumpAnim.next();
            }
            else if (atkAnim > 0)
            {
                loc.X -= 4;
            }
            atkAnim--;
        }
        public int attack()
        {

            if (isAttacking)
            {
                hud.tP--;
                atkAnim = 50;
                Random rand = new Random();
                int chance = rand.Next(100);
                if (chance < hud.acc)
                {
                    
                    return hud.atk;
                }
                hud.tP--;
                
            }
            
            return 0;
        }
        public int damaged(int dmg)
        {
            int c = dmg - hud.def;
            if (c < 0)
            {
                return 0;
            }
            hud.hP -= c;
            if (hud.hP < 0)
            {
                hud.hP = 0;
            }
            return c;
        }
        public Boolean alive()
        {
            if (hud.hP <= 0)
            {
                return false;
            }
            return true;
        }
        public void reset()
        {
            lives--;
            if (lives == 0)
            {
                gameOver = true;
            }
        }
        
        public void draw(GameTime gameTime, SpriteBatch spriteBatch, Room curr)
        {
            if (inBattle)
            {
                if (faint)
                {
                    spriteBatch.Draw(faintAnim, loc, Color.White);
                }
                else if (hurt)
                {
                    spriteBatch.Draw(hurtAnim, loc, Color.White);
                }
                else if (atkAnim > 20 && atkAnim <= 30)
                {
                    spriteBatch.Draw(jumpAnim.getFrame(), loc, Color.White);
                }
                else 
                {
                    spriteBatch.Draw(idleAnim.getFrame(), loc, Color.White);
                }
            }
            else if (isRight)
            {
                if (isJumping)
                {
                    spriteBatch.Draw(jumpAnim.getFrame(), loc, Color.White);
                }
                else if (isCrouching)
                {
                    spriteBatch.Draw(crouchAnim.getFrame(), loc, Color.White);
                }
                else if (vel.X > -VelStop && vel.X < VelStop)
                {
                    spriteBatch.Draw(idleAnim.getFrame(), loc, Color.White);
                }
                else
                {
                    spriteBatch.Draw(walkAnim.getFrame(), loc, Color.White);
                }
            }
            else
            {
                if (isJumping)
                {
                    spriteBatch.Draw(jumpAnim.getFrame(), loc, null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1f);
                }
                else if (isCrouching)
                {
                    spriteBatch.Draw(crouchAnim.getFrame(), loc, null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1f);
                }
                else if (vel.X > -VelStop && vel.X < VelStop)
                {
                    spriteBatch.Draw(idleAnim.getFrame(), loc, null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1f);
                }
                else
                {
                    spriteBatch.Draw(walkAnim.getFrame(), loc, null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.FlipHorizontally, 1f);
                }
            }
            displayGUI(gameTime, spriteBatch, curr);
            hud.Draw(gameTime, spriteBatch);
        }

        public void displayGUI(GameTime gameTime, SpriteBatch spriteBatch, Room curr)
        {
            enclosedDrawString(spriteBatch, debugFont, "[" + (camera.loc.X + loc.X) + ", " + (camera.loc.Y + loc.Y) + "]", new Vector2(10, 160), new Color(0, 255, 0), Color.Green);
            float frameRate = 1 / (float)gameTime.ElapsedGameTime.TotalSeconds;
            try
            {
                enclosedDrawString(spriteBatch, debugFont, "FPS: " + Math.Round(frameRate, 1), new Vector2(10, 180), new Color(0, 255, 0), Color.Green);
            }
            catch (ArgumentException e)
            {
                enclosedDrawString(spriteBatch, debugFont, "FPS: 0", new Vector2(10, 180), new Color(0, 255, 0), Color.Green);
            }
            enclosedDrawString(spriteBatch, debugFont, "Map: " + curr.num, new Vector2(10, 200), new Color(0, 255, 0), Color.Green);
        }

        public void enclosedDrawString(SpriteBatch spriteBatch, SpriteFont font, string message, Vector2 position, Color c1, Color c2)
        {
            spriteBatch.DrawString(font, message, new Vector2(position.X - 1, position.Y), c2);
            spriteBatch.DrawString(font, message, new Vector2(position.X - 1, position.Y - 1), c2);
            spriteBatch.DrawString(font, message, new Vector2(position.X, position.Y - 1), c2);
            spriteBatch.DrawString(font, message, new Vector2(position.X + 1, position.Y - 1), c2);
            spriteBatch.DrawString(font, message, new Vector2(position.X + 1, position.Y), c2);
            spriteBatch.DrawString(font, message, new Vector2(position.X + 1, position.Y + 1), c2);
            spriteBatch.DrawString(font, message, new Vector2(position.X, position.Y + 1), c2);
            spriteBatch.DrawString(font, message, new Vector2(position.X - 1, position.Y + 1), c2);
            spriteBatch.DrawString(font, message, position, c1);
        }
        
    }
}