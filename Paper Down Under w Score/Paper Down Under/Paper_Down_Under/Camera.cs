﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Paper_Down_Under
{
    public class Camera
    {
        public Rectangle loc;
        public Rectangle viewport;

        public Camera()
        {

        }

        public Camera(Rectangle _loc, Rectangle _viewport)
        {
            loc = _loc;
            viewport = _viewport;
        }
    }
}
