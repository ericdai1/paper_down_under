﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Paper_Down_Under
{
    public class Tile
    {
        public enum TileState
        {
            Impassable,
            Fluid,
            Passable,
            Platform,
            Start
        }

        private Texture2D image;
        private Rectangle loc;
        private TileState isSolid;
        private Camera camera;

        public Tile()
        {

        }

        public Tile(Rectangle _loc, Texture2D _image, TileState _isSolid, Camera _camera)
        {
            loc = _loc;
            image = _image;
            isSolid = _isSolid;
            camera = _camera;
        }

        public Rectangle _loc
        {
            get
            {
                return loc;
            }
        }

        public Texture2D _image
        {
            get
            {
                return image;
            }
        }

        public TileState _state
        {
            get
            {
                return isSolid;
            }
        }

        public void draw(GameTime gameTime, SpriteBatch spriteBatch, Camera camera)
        {
            if (image != null && loc.Intersects(camera.loc))
            {
                spriteBatch.Draw(image, new Rectangle(loc.X - camera.loc.X, loc.Y - camera.loc.Y, loc.Width, loc.Height), Color.White);
            }
        }

        public override string ToString()
        {
            return $"Image:{_image}, Loc:{_loc}, State:{_state}";
        }
    }
}
