using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.IO;

namespace Paper_Down_Under
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public enum GameState
        {
            Play,
            Menu,
            Legend,
            GameOver
        }
        public enum GameStatus
        {
            Platform,
            Battle
        }
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Enemy bee;
        Animation beeAnim;
        Texture2D beeText;
        Texture2D blur;
        Texture2D legend;
        Dictionary<string, Tile> tileInterpret;
        List<Room> rooms;
        Room currRoom;
        Player player;
        public const int TileLength = 64;
        Camera camera;
        Random random;
        PlayerHUD hud;
        SpriteFont sf;
        Dictionary<string, Animation> animationLib;
        Dictionary<string, Enemy.Movement> movementLib;
        SoundEffectInstance music;
        SoundEffectInstance ambience;
        SoundEffectInstance mystery;
        Enemy fightE;
        private GameStatus gS;
        public GameState state;
        int legendTicks;
        const int MAX_LEGEND_TICKS = 300;

        Texture2D fight1;
        Texture2D fight2;
        Rectangle fightRect;
        Texture2D item1;
        Texture2D item2;
        Rectangle itemRect;
        Texture2D flee1;
        Texture2D flee2;
        Rectangle fleeRect;
        Texture2D textBoxT;
        Rectangle textBoxR;
        Texture2D heartIcon;
        Boolean highlightFight;
        Boolean highlightItem;
        Boolean highlightFlee;
        String text;
        String[] words;
        String text2;
        KeyboardState oldKB = Keyboard.GetState();
        SpriteFont sF2;
        SpriteFont sF1;
        Boolean continued;
        int time;
        int ticks;
        int textTime;
        int fleeTimer;
        int cooldown;
        int score;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        
        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 1200;
            graphics.PreferredBackBufferHeight = 675;
            graphics.ApplyChanges();
            gS = GameStatus.Platform;
            state = GameState.Legend;
            camera = new Camera(GraphicsDevice.Viewport.Bounds, GraphicsDevice.Viewport.Bounds);
            rooms = new List<Room>();
            tileInterpret = new Dictionary<string, Tile>();
            animationLib = new Dictionary<string, Animation>();
            movementLib = new Dictionary<string, Enemy.Movement>();
            base.Initialize();
            IsMouseVisible = true;
            fightRect = new Rectangle(20, 360, 250, 100);
            itemRect = new Rectangle(20, 470, 250, 100);
            fleeRect = new Rectangle(20, 580, 250, 100);
            textBoxR = new Rectangle(300, 500, 750, 175);
            score = 0;
            legendTicks = 0;
            time = 0;
            ticks = 100;
            textTime = 0;
            fleeTimer = 0;
            cooldown = 0;
            highlightFight = false;
            highlightItem = false;
            highlightFlee = false;
            text = "";
            text2 = "";
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            music = Content.Load<SoundEffect>("Music/Bamboo-Forest").CreateInstance();
            ambience = Content.Load<SoundEffect>("Music/Forest_Ambience").CreateInstance();
            mystery = Content.Load<SoundEffect>("Music/Sword Of Mystery").CreateInstance();
            music.IsLooped = true;
            ambience.IsLooped = true;
            mystery.IsLooped = true;

            blur = Content.Load<Texture2D>("Sprites/Blur");
            beeText = this.Content.Load<Texture2D>("Sprites/Bee Sprite Sheet");
            legend = Content.Load<Texture2D>("Sprites/Legend");
            readTileInterpreter(@"Content/Tiles/convert.txt");
            LoadEnemies();
            for (int i = 0; i < Int32.MaxValue; i++)
            {
                try
                {
                    readMap(@"Content/Maps/Map" + i + ".map");
                }
                catch (FileNotFoundException e)
                {
                    break;
                }
            }
            for (int i = 0; i < Int32.MaxValue; i++)
            {
                try
                {
                    readEnemy(@"Content/Maps/Map" + i + "Enemies.dat", i);
                }
                catch (FileNotFoundException e)
                {
                    break;
                }
            }

            currRoom = rooms[0];

            Texture2D idle = Content.Load<Texture2D>("Sprites/Player Generic Idle");
            Texture2D walk = Content.Load<Texture2D>("Sprites/Player Generic Walk");
            Texture2D jump = Content.Load<Texture2D>("Sprites/Player Generic Jump");
            Texture2D crouch = Content.Load<Texture2D>("Sprites/Player Generic Crouch");
            Texture2D hurt = Content.Load<Texture2D>("Sprites/Player Generic Hurt");
            Texture2D faint = Content.Load<Texture2D>("Sprites/Player Generic Faint");
            SpriteFont debugFont = Content.Load<SpriteFont>("Fonts/DebugFont");
            //for (int r = 0; r < currRoom.tileMap.Length; r++)
            //{
            //    for (int c = 0; c < currRoom.tileMap[0].Length; c++)
            //    {
            //        if (currRoom.tileMap[r][c] != null && currRoom.tileMap[r][c]._state == Tile.TileState.Start)
            //        {
            //            camera.loc.X = c + GraphicsDevice.Viewport.Width / 4;
            //            camera.loc.Y = r + GraphicsDevice.Viewport.Height / 2;
            //            break;
            //        }
            //    }
            //}
            player = new Player(new Animation(idle, GraphicsDevice, new Rectangle(0, 0, idle.Height, idle.Height), 10, 16),
                new Animation(walk, GraphicsDevice, new Rectangle(0, 0, walk.Height, walk.Height), 5, 13),
                new Animation(jump, GraphicsDevice, new Rectangle(0, 0, jump.Height, jump.Height), 6, 10),
                new Animation(crouch, GraphicsDevice, new Rectangle(0, 0, crouch.Height, crouch.Height), 6, 8), hurt, faint, debugFont,
                new Rectangle(GraphicsDevice.Viewport.Bounds.Center.X - TileLength / 2, GraphicsDevice.Viewport.Bounds.Center.Y - TileLength / 2, TileLength, TileLength),
                camera, new Vector2(5, 15), new Vector2(.7f, .8f));
            camera.loc.X = 945 - camera.loc.Width / 2;
            camera.loc.Y = 1599 - camera.loc.Height / 2;
            hud = new PlayerHUD(100, 5, 90, 3, 11);
            heartIcon = this.Content.Load<Texture2D>("Sprites/HeartIcon");
            hud.LC(this.Content.Load<SpriteFont>("Fonts/SpriteFont1"), this.Content.Load<Texture2D>("Sprites/HPbar"), this.Content.Load<Texture2D>("Sprites/TPBar"), heartIcon,
                this.Content.Load<Texture2D>("Sprites/TPIcon"), this.Content.Load<Texture2D>("Sprites/goldIcon"), this.Content.Load<Texture2D>("Sprites/Player Generic Head"), this.Content.Load<Texture2D>("Sprites/Player Generic Back Head"));
            player.InitializeHUD(hud);
            fight1 = this.Content.Load<Texture2D>("Sprites/Fight1");
            fight2 = this.Content.Load<Texture2D>("Sprites/Fight2");
            item1 = this.Content.Load<Texture2D>("Sprites/Item1");
            item2 = this.Content.Load<Texture2D>("Sprites/Item2");
            flee1 = this.Content.Load<Texture2D>("Sprites/Flee1");
            flee2 = this.Content.Load<Texture2D>("Sprites/Flee2");
            textBoxT = this.Content.Load<Texture2D>("Sprites/TextBox");
            sF1 = this.Content.Load<SpriteFont>("Fonts/SpriteFont1");
            sF2 = this.Content.Load<SpriteFont>("Fonts/SpriteFont2");
            mystery.Play();
        }

        //will eventually turn into loading only the enemies & animations for the current area
        protected void LoadEnemies()
        {
            LoadAnimations();
            random = new Random();
        }

        protected void LoadAnimations()
        {
            int[] beeImgs = new int[6];
            for (int i = 0; i < 6; i++)
            {
                beeImgs[i] = 2;
            }
            beeAnim = new Animation(beeText, GraphicsDevice, new Rectangle(0, 0, 200, 140), beeImgs);
            movementLib.Add("Wander", Enemy.Movement.Wander);
            movementLib.Add("Follow", Enemy.Movement.Follow);
            animationLib.Add("beeAnim", beeAnim);
        }

        private void readTileInterpreter(string path)
        {
            try
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    while (!reader.EndOfStream)
                    {
                        string[] data = reader.ReadLine().Split(' ');
                        string[] size = data[data.Length - 1].Split('x');
                        int width = Convert.ToInt32(size[0]);
                        int height = Convert.ToInt32(size[1]);
                        if (data[1].Equals("none"))
                        {
                            tileInterpret.Add(data[0], new Tile(new Rectangle(0, 0, width * TileLength, height * TileLength), null, Tile.TileState.Passable, camera));
                        }
                        else
                        {
                            Texture2D img = Content.Load<Texture2D>("Tiles/Forest/" + data[1]);
                            switch (data[2])
                            {
                                case "solid":
                                    tileInterpret.Add(data[0], new Tile(new Rectangle(0, 0, width * TileLength, height * TileLength), img, Tile.TileState.Impassable, camera));
                                    break;
                                case "passa":
                                    tileInterpret.Add(data[0], new Tile(new Rectangle(0, 0, width * TileLength, height * TileLength), img, Tile.TileState.Passable, camera));
                                    break;
                                case "fluid":
                                    tileInterpret.Add(data[0], new Tile(new Rectangle(0, 0, width * TileLength, height * TileLength), img, Tile.TileState.Fluid, camera));
                                    break;
                                case "platform":
                                    tileInterpret.Add(data[0], new Tile(new Rectangle(0, 0, width * TileLength, height * TileLength), img, Tile.TileState.Platform, camera));
                                    break;
                                case "start":
                                    tileInterpret.Add(data[0], new Tile(new Rectangle(0, 0, width * TileLength, height * TileLength), img, Tile.TileState.Start, camera));
                                    break;
                            }
                        }
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("The tile interpetation file could not be found:");
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Reads in the room specified by the file path.
        /// </summary>
        private void readMap(string mapPath)
        {
            using (StreamReader reader = new StreamReader(mapPath))
            {
                List<List<string>> mapData = new List<List<string>>();
                Texture2D background = Content.Load<Texture2D>("Backgrounds/" + reader.ReadLine());
                string[] dirsData = reader.ReadLine().Split(' ');
                int[] dirs = new int[dirsData.Length];
                for (int i = 0; i < dirsData.Length; i++)
                {
                    dirs[i] = Convert.ToInt32(dirsData[i]);
                }
                while (!reader.EndOfStream)
                {
                    string[] line = reader.ReadLine().Split(' ');
                    mapData.Add(new List<string>(line));
                }
                Tile[][] map = new Tile[mapData.Count][];
                for (int r = 0; r < mapData.Count; r++)
                {
                    map[r] = new Tile[mapData[r].Count];
                    for (int c = 0; c < mapData[r].Count - 1; c++)
                    {
                        Tile convTile = tileInterpret[mapData[r][c]];
                        map[r][c] = new Tile(new Rectangle(r * TileLength, c * TileLength, convTile._loc.Width, convTile._loc.Height), convTile._image, convTile._state, camera);
                    }
                }
                rooms.Add(new Room(rooms.Count, dirs[0], dirs[1], dirs[2], dirs[3], background, map, camera));
            }
        }

        /// <summary>
        /// Reads in the room specified by the file path.
        /// </summary>
        private void readEnemy(string enemyPath, int room)
        {
            // 10 0 95 10 5 Hollow_Bee beeAnim 500 100 200 140 Wander Follow 400 1 1
            // int h, int d, int ac, int at, int g, string n, Animation _idleAnim, Rectangle _loc, Camera _camera, Movement moFar, Movement moClo, int pro, Vector2 bv, Random ran
            using (StreamReader reader = new StreamReader(enemyPath))
            {
                while (!reader.EndOfStream)
                {
                    Enemy enemy;
                    string[] line = reader.ReadLine().Split(' ');
                    enemy = new Enemy(Convert.ToInt32(line[0]), Convert.ToInt32(line[1]), Convert.ToInt32(line[2]), Convert.ToInt32(line[3]), Convert.ToInt32(line[4]), Convert.ToInt32(line[5]), line[6].Split('_').ToString(), animationLib[line[7]],
                                        new Rectangle(Convert.ToInt32(line[8]), Convert.ToInt32(line[9]), Convert.ToInt32(line[10]), Convert.ToInt32(line[11])), camera, movementLib[line[12]], movementLib[line[13]], Convert.ToInt32(line[14]),
                                        new Vector2((float)Convert.ToDouble(line[15]), (float)Convert.ToDouble(line[16])), random);
                    rooms[room].enemies.Add(enemy);
                }
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            KeyboardState kB = Keyboard.GetState();
            if (player.lives <= 0)
            {
                state = GameState.GameOver;
            }
            if (state == GameState.Play)
            {
                
                if (gS == GameStatus.Platform)
                {
                    if (currRoom.left >= 0 && camera.loc.X + player._loc.X <= 0)
                    {
                        currRoom = rooms[currRoom.left];
                        camera.loc = new Rectangle(currRoom.tileMap.Length * TileLength - camera.loc.Width / 2 - TileLength, camera.loc.Y, camera.loc.Width, camera.loc.Height);
                        currRoom.enemies = new List<Enemy>();
                        try
                        {
                            readEnemy(@"Content/Maps/Map" + currRoom.num + "Enemies.dat", currRoom.num);
                        }
                        catch (FileNotFoundException e)
                        {
                            Console.WriteLine("File reading done");
                        }
                    }
                    else if (currRoom.right >= 0 && camera.loc.X + player._loc.X > currRoom.tileMap.Length * TileLength - TileLength)
                    {
                        currRoom = rooms[currRoom.right];
                        camera.loc = new Rectangle(TileLength - camera.loc.Width / 2, camera.loc.Y, camera.loc.Width, camera.loc.Height);
                        currRoom.enemies = new List<Enemy>();
                        try
                        {
                            readEnemy(@"Content/Maps/Map" + currRoom.num + "Enemies.dat", currRoom.num);
                        }
                        catch (FileNotFoundException e)
                        {
                            Console.WriteLine("File reading done");
                        }
                    }
                    else if (currRoom.up >= 0 && camera.loc.Y + player._loc.Y < 0)
                    {
                        currRoom = rooms[currRoom.up];
                        camera.loc = new Rectangle(currRoom.tileMap.Length * TileLength - camera.loc.Width / 2 - 2 * TileLength,
                            currRoom.tileMap[0].Length * TileLength - camera.loc.Height / 2 - 2 * TileLength, camera.loc.Width, camera.loc.Height);
                        currRoom.enemies = new List<Enemy>();
                        try
                        {
                            readEnemy(@"Content/Maps/Map" + currRoom.num + "Enemies.dat", currRoom.num);
                        }
                        catch (FileNotFoundException e)
                        {
                            Console.WriteLine("File reading done");
                        }
                    }
                    else if (currRoom.down >= 0 && camera.loc.Y + player._loc.Y > currRoom.tileMap[0].Length * TileLength - TileLength)
                    {
                        currRoom = rooms[currRoom.down];
                        camera.loc = new Rectangle(currRoom.tileMap.Length * TileLength - camera.loc.Width / 2 - 2 * TileLength,
                            TileLength - camera.loc.Height / 2, camera.loc.Width, camera.loc.Height);
                        currRoom.enemies = new List<Enemy>();
                        try
                        {
                            readEnemy(@"Content/Maps/Map" + currRoom.num + "Enemies.dat", currRoom.num);
                        }
                        catch (FileNotFoundException e)
                        {
                            Console.WriteLine("File reading done");
                        }
                    }
                }

                //Below is important Battle Interface code
                fleeTimer--;
                if (gS == GameStatus.Platform)
                {

                    player.update(gameTime, currRoom);
                    if (fleeTimer < 0)
                    {
                        fightE = currRoom.intercept(player);
                        if (fightE != null)
                        {
                            gS = GameStatus.Battle;
                            text = "Choose your move...";
                            textTime = 0;
                            player.turn = true;
                            player.inBattle = true;
                            currRoom.setRect(player._loc);
                        }
                    }
                }

                if (gS == GameStatus.Battle)
                {

                    player.battleAnim(gameTime, currRoom);
                    if (textTime == 0)
                    {
                        words = text.Split(' ');
                        text2 = "";
                    }
                    if (textTime % 5 == 0 && textTime / 5 < words.Length)
                    {
                        text2 += words[textTime / 5] + " ";
                    }
                    textTime++;
                    if (kB.IsKeyDown(Keys.Space) && !oldKB.IsKeyDown(Keys.Space))
                    {
                        continued = true;
                    }
                    ticks++;
                    cooldown--;

                    if ((continued || player.isAttacking || player.isRetreating) && cooldown < 0)
                    {
                        
                        
                        ticks = 100;
                        time--;
                        continued = false;
                        player.hurt = false;
                        highlightFight = false;
                        highlightItem = false;
                        highlightFlee = false;
                        if (!player.alive() && time <= 1)
                        {
                            player.lives--;
                            text = "You have fainted... and now have " + player.lives + " lives remaining. \n\nPress space to respawn. ";
                            textTime = 0;
                            time = 5;
                            fleeTimer = 180;
                            currRoom.setTurn(false);
                            player.turn = false;
                            player.faint = true;
                            if (player.lives <= 0)
                            {
                                state = GameState.GameOver;
                            }
                            cooldown = 40;
                            
                        }
                        else if (!currRoom.enemyLife() && time <= 1)
                        {
                            text = "The Hollow Bee has become exhausted. \nYou recieved " + fightE.gold + " gold and " + fightE.exp + " exp as a reward! \n\nPress space to exit the fight.";
                            textTime = 0;
                            player.hud.tP += 2;
                            player.hud.gold += fightE.gold;
                            player.hud.exp += fightE.exp;
                            time = 3;
                            currRoom.setTurn(false);
                            player.turn = false;
                            score += fightE.maxHP * fightE.maxHP;
                            cooldown = 40;
                        }
                        else if (time == 2)
                        {
                            gS = GameStatus.Platform;
                            player.inBattle = false;
                            currRoom.noBattle();
                            time = -1;
                            fleeTimer = 100;

                        }
                        else if (time == 4)
                        {
                            gS = GameStatus.Platform;
                            player.inBattle = false;
                            currRoom.noBattle();
                            player.hud.death();
                            currRoom = rooms[0];
                            camera.loc.X = 945 - camera.loc.Width / 2;
                            camera.loc.Y = 1599 - camera.loc.Height / 2;
                            time = -1;
                            player.faint = false;
                        }

                        if (player.isAttacking)
                        {
                            if (player.hud.tP <= 0)
                            {
                                player.hud.tP = 0;
                                text = "You are out of tension points... Try another move";
                                textTime = 0;

                                player.isAttacking = false;
                            }
                            else
                            {
                                int dmg = 0;
                                dmg = player.attack();
                                if (dmg <= 0)
                                {
                                    text = "Your attack missed the opponent :( \n\nPress space to continue...";
                                    textTime = 0;
                                }
                                else
                                {
                                    dmg = currRoom.dmgDealt(dmg);
                                    if (fightE.hP < 0)
                                    {
                                        fightE.hP = 0;

                                    }
                                    text = "Your attack dealt " + dmg + " to the opponent! \nThe Hollow Bee has " + fightE.hP + " HP remaining.";
                                    textTime = 0;
                                }
                                time = 1;
                                player.isAttacking = false;
                                currRoom.setTurn(true);
                                player.turn = false;

                            }
                            highlightFight = true;
                            cooldown = 40;
                        }
                        else if (player.isRetreating)
                        {
                            Random rand = new Random();

                            if (rand.Next(100) < 60)
                            {
                                text = "You successfully fleed from this fight! \n\nPress space to continue...";
                                textTime = 0;

                                time = 3;
                            }
                            else
                            {
                                text = "You unsuccessfully tried to flee! \n\nPress space to continue...";
                                textTime = 0;
                                time = 1;
                            }
                            currRoom.setTurn(true);
                            player.turn = false;
                            highlightFlee = true;
                            player.isRetreating = false;
                            cooldown = 40;
                        }
                        else if (player.turn)
                        {
                            text = "Choose your move...";
                            textTime = 0;
                        }
                        else if (time == 0 && currRoom.isTurn())
                        {
                            text = "The Hollow Bee is ready to attack. \n\nPress space to continue...";
                            textTime = 0;
                            cooldown = 40;
                        }
                        else if (currRoom.isTurn())
                        {
                            int dmg = currRoom.enemyFight(gameTime);
                            if (dmg <= 0)
                            {
                                text = "The Hollow Bee missed their attack! \n\nPress space to continue...";
                                textTime = 0;
                            }
                            else
                            {
                                int outp = player.damaged(dmg);
                                text = "The Hollow Bee dealt " + outp + " damage to you! \nYou now have " + player.hud.hP + " health remaining. \n\nPress space to continue...";
                                textTime = 0;
                                ticks = 0;
                            }
                            if (player.alive() && currRoom.enemyLife())
                            {
                                player.turn = true;
                                currRoom.setTurn(false);
                            }
                            cooldown = 40;
                        }


                    }
                    if (ticks > 40 && ticks < 100)
                    {
                        player.hurt = true;
                    }
                    else
                    {
                        player.hurt = false;
                    }
                    oldKB = kB;

                }
            }
            else if (state == GameState.Legend)
            {
                if (legendTicks == 270)
                {
                    mystery.Stop();
                }
                if (legendTicks < MAX_LEGEND_TICKS)
                {
                    legendTicks++;
                }
                else
                {
                    state = GameState.Play;
                    music.Play();
                    ambience.Play();
                }
            }

            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            if (state == GameState.Play)
            {
                currRoom.draw(player, gameTime, spriteBatch);
                currRoom.draw(player, gameTime, spriteBatch);
                if (currRoom.num != 3)
                {
                    spriteBatch.Draw(blur, GraphicsDevice.Viewport.Bounds, Color.Black);
                }
                if (currRoom.num == 4)
                {
                    spriteBatch.Draw(blur, GraphicsDevice.Viewport.Bounds, Color.Black);
                }
                player.draw(gameTime, spriteBatch, currRoom);
                spriteBatch.DrawString(sF1, "Score: " + score, new Vector2(950, 60), Color.White);
                //if (currRoom.num == 0)
                //{
                //    spriteBatch.Draw(blur, GraphicsDevice.Viewport.Bounds, Color.Black);
                //}
                if (gS == GameStatus.Battle)
                {

                    if (highlightFight)
                        spriteBatch.Draw(fight2, fightRect, Color.White);
                    else
                        spriteBatch.Draw(fight1, fightRect, Color.White);
                    if (highlightFlee)
                        spriteBatch.Draw(flee2, fleeRect, Color.White);
                    else
                        spriteBatch.Draw(flee1, fleeRect, Color.White);
                    if (highlightItem)
                        spriteBatch.Draw(item2, itemRect, Color.White);
                    else
                        spriteBatch.Draw(item1, itemRect, Color.White);

                    spriteBatch.Draw(textBoxT, textBoxR, Color.White);
                    spriteBatch.DrawString(sF2, text2, new Vector2(330, 530), Color.White);
                    currRoom.battleDraw(gameTime, spriteBatch, sF2);
                    Rectangle temp = currRoom.getRect();
                    spriteBatch.Draw(heartIcon, new Rectangle(temp.X - 10, temp.Y - 30, 30, 30), Color.White);
                }
            }
            else if (state == GameState.Legend)
            {
                if (legendTicks < 60)
                {
                    spriteBatch.Draw(legend, GraphicsDevice.Viewport.Bounds, Color.White * ((float)legendTicks / 60));
                }
                else if (legendTicks > MAX_LEGEND_TICKS - 60)
                {
                    spriteBatch.Draw(legend, GraphicsDevice.Viewport.Bounds, Color.White * ((float)(MAX_LEGEND_TICKS - legendTicks) / 60));
                }
                else
                {
                    spriteBatch.Draw(legend, GraphicsDevice.Viewport.Bounds, Color.White);
                }
            }
            else if (state == GameState.GameOver)
            {
                KeyboardState kB = Keyboard.GetState();
                if (kB.IsKeyDown(Keys.Escape))
                {
                    this.Exit();
                }
                spriteBatch.DrawString(sF1, "GAME OVER! Better luck next time! \n Score: " + score + "\n Press ESC to quit...", new Vector2(250, 250), Color.White);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}